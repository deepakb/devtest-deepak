(function() {

    var app = angular.module('notesApp', ['ngRoute', 'ngMaterial']);

    app.config(['$locationProvider', '$routeProvider',
        function($locationProvider, $routeProvider) {

            $routeProvider
                .when('/', {
                    templateUrl: '/partials/notes-view.html',
                    controller: 'notesController'
                })
                .when('/login', {
                    templateUrl: '/partials/login.html',
                    controller: 'loginController',
                })

                .otherwise('/');
        }
    ]);

    app.run(['$rootScope', '$location', 'AuthService', function($rootScope, $location, AuthService) {
        $rootScope.$on('$routeChangeStart', function(event) {

            AuthService.checkForExistingUser();

            if ($location.path() == "/login") {
                if (AuthService.isLoggedIn()) {
                    $location.path('/');
                }
                return;
            }

            if ($location.path() == "/logout") {
                AuthService.logout();
                $location.path('/login');
            }

            if (!AuthService.isLoggedIn()) {
                console.log('DENY');
                event.preventDefault();
                $location.path('/login');
            }
        });


        $rootScope.logout = function() {
            AuthService.logout()
            $location.path('/login');
        }

    }]);


    app.service('AuthService', function($http) {
        var loggedUser = null;


        function logout() {
            loggedUser = null;
            window.localStorage.removeItem("notesApp.user")
        }

        function checkForExistingUser() {
            var item = window.localStorage.getItem("notesApp.user");
            console.log(item)
            if (item !== undefined) {
                var user = JSON.parse(item)
                loggedUser = user;
            }
        }


        function login(username, password) {
            return $http.post("api/login", {
                username: username,
                password: password
            }).then(function(user) {
                loggedUser = user;
                return user;
            }, function(error) {
                loggedUser = null;
                throw "user not available"
            })
        }

        function isLoggedIn() {
            return loggedUser != null;
        }
        return {
            login: login,
            isLoggedIn: isLoggedIn,
            checkForExistingUser: checkForExistingUser,
            logout: logout
        }
    });

    app.controller('loginController', function($scope, AuthService, $location) {

        $scope.invalidCreds = false;
        $scope.login = {
            username: null,
            password: null
        };

        $scope.login = function() {




            console.log($scope.loginForm.$valid)

            if ($scope.loginForm.$valid) {
                AuthService.login($scope.login.username, $scope.login.password).then(function(user) {
                    window.localStorage.setItem("notesApp.user", JSON.stringify(user))
                    $location.path("/");
                }, function(error) {
                    console.log(error);
                    $scope.invalidCreds = true;
                });
            }


        };
    });


    app.controller('notesController', function($scope, $http) {

        $scope.isEditCreateView = false;
        $scope.mode = null; //create or edit

        $scope.notes = [{
            id: 1,
            "name": "Test",
            "body": "Note body"
        }]
        $scope.note = newNote()

        function newNote() {
            return {
                id: null,
                name: null,
                body: null
            }
        }


        this.init = function() {
            console.log("fetchList")
            fetchList()
        }

        fetchList = function() {
            $http.get("/api/notes")
                .then(function(response) {
                    $scope.notes = response.data
                })
        }


        $scope.newNoteView = function() {
            $scope.note = newNote()
            $scope.isEditCreateView = true;
            $scope.mode = "create"
        };

        $scope.deleteNote = function(i) {
            var r = confirm("Are you sure you want to delete this note?");
            if (r == true) {
                //TODO delete the note
            }
        };

        $scope.viewNote = function() {
            //TODO
        }

        $scope.editNote = function(note) {
            $scope.isEditCreateView = true;
            $scope.note = note
            $scope.mode = "edit"

        }

        $scope.cancel = function() {

            $scope.isEditCreateView = false;
            $scope.mode = null;

        }

        $scope.process = function(form) {


            if (form.$valid) {

                if ($scope.mode == "create") {
                    $http.post('/api/notes', $scope.note)
                        .then(function(res) {
                            $scope.isEditCreateView = false;
                            $scope.mode = null;
                            $scope.notes.push(res.data)
                        }, function(err) {
                            console.log(err)
                        })
                } else {
                    $http.post('/api/notes/' + $scope.note.id, $scope.note)
                        .then(function(res) {
                            $scope.isEditCreateView = false;
                            $scope.mode = null;
                            var index = $scope.notes.findIndex(item => item.id == $scope.note.id)
                            $scope.notes[index] = res.data
                        }, function(err) {
                            console.log(err)
                        })
                }
            }


        }

        this.init()
    });

})();