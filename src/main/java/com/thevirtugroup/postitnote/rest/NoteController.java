package com.thevirtugroup.postitnote.rest;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 *
 */
@RestController
public class NoteController {

    private NoteRepository noteRepository;

    @Autowired
    public NoteController(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/notes")
    ResponseEntity<Note> save(@RequestBody Note note) {
        note = noteRepository.save(note);
        return new ResponseEntity<>(note, HttpStatus.valueOf(200));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/notes")
    ResponseEntity<List<Note>> getAll() {
        List<Note> list = noteRepository.getAll();
        return new ResponseEntity<List<Note>>(list, HttpStatus.valueOf(200));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/notes/{noteId}")
    ResponseEntity<? extends Object> update(@PathVariable Long noteId, @RequestBody Note note) {
        Note updatedNote = noteRepository.update(note);
        if (updatedNote == null) {
            return new ResponseEntity<ErrorResponse>(new ErrorResponse("Note not found"), HttpStatus.valueOf(404));
        } else {
            return new ResponseEntity<Note>(updatedNote, HttpStatus.valueOf(200));
        }
    }

}
