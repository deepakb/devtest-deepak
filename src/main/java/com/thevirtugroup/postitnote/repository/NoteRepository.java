package com.thevirtugroup.postitnote.repository;


import com.thevirtugroup.postitnote.model.Note;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class NoteRepository {

    private List<Note> notes;
    private Long lastId;

    public NoteRepository() {
        this.notes = new ArrayList<>();

        // Sample data.
        Note testNote = new Note(1L, "Test note", "This is a test note from server.");
        notes.add(testNote);
        lastId = 1L;

    }

    private Long getNextId() {
        lastId = lastId + 1;
        return lastId;
    }

    public Note save(Note note) {
        note.setId(getNextId());
        notes.add(note);
        return note;
    }

    public Optional<Note> getById(Long id) {
        return notes.stream()
                .filter(note -> note.getId() == id)
                .findFirst();
    }

    public List<Note> getAll() {
        return notes;
    }

    /**
     * Update
     * @param note
     * @return
     */
    public Note update(Note note) {
        int index = -1;
        int i = 0;
        for (Note item : notes) {
            if (item.getId() == note.getId()) {
                index = i;
                break;
            }
            i++;
        }
        if (index == -1) {
            return null;
        } else {
            notes.set(index, note);
        }
        return note;
    }

}
